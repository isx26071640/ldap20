#! /bin/bash
ulimit -n 1024
function initdb(){
	rm -rf /etc/openldap/slapd.d/*
        rm -rf /var/lib/ldap/*
        cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
        slaptest -f slapd.conf -F /etc/openldap/slapd.d/
        slaptest -f slapd.conf -F /etc/openldap/slapd.d/ -u
        slaptest -F /etc/openldap/slapd.d/ -u
        chown -R ldap.ldap /etc/openldap/slapd.d/
        chown -R ldap.ldap /var/lib/ldap/
        cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
        /sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"
}
function initdbedt(){
	rm -rf /etc/openldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
	slaptest -f slapd.conf -F /etc/openldap/slapd.d/
	slaptest -f slapd.conf -F /etc/openldap/slapd.d/ -u
	slaptest -F /etc/openldap/slapd.d/ -u
	slapadd -F /etc/openldap/slapd.d/ -l /opt/docker/organitzacio-edt.org.ldif
	slapadd -F /etc/openldap/slapd.d/ -l /opt/docker/usuaris-edt.org.ldif
	chown -R ldap.ldap /etc/openldap/slapd.d/
	chown -R ldap.ldap /var/lib/ldap/
	cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
	/sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"
}
function start(){
	/sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"
}
case $1 in
	initdbedt)
		echo "initdbedt"
		initdbedt;;
	start)
		echo "start"
		start;;
	initdb)
		echo "initdb"
		initdb;;
	*) 
		echo "altre"
		start
			
esac
#bash /opt/docker/install.sh
#/sbin/slapd -d0 ldap:// ldaps:// ldapi://
